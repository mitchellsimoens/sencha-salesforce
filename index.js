module.exports = {
    get Connection () {
        return require('./Connection');
    },

    get Manager () {
        return require('./Manager');
    }
};
